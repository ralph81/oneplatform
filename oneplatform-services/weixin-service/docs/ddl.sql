DROP TABLE IF EXISTS `weixin_configs`;
CREATE TABLE `weixin_configs` (
  `id` int(10)  NOT NULL AUTO_INCREMENT,
  `tenant_id` varchar(32)  NOT NULL, 
  `type` varchar(32) DEFAULT NULL,
  `app_id` varchar(32) DEFAULT NULL,
  `app_secret` varchar(32) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT 1,
  `deleted` tinyint(1) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='微信配置';

DROP TABLE IF EXISTS `weixin_bindings`;
CREATE TABLE `weixin_bindings` (
  `id` int(10)  NOT NULL AUTO_INCREMENT,
  `user_id` int(10)  NOT NULL, 
  `open_type` ENUM('mp', 'weapp') DEFAULT NULL,
  `open_id` varchar(32) DEFAULT NULL,
  `union_id` varchar(32) DEFAULT NULL,
  `source_app` varchar(32) DEFAULT NULL,
  `enabled` bit(1) DEFAULT b'1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
   UNIQUE INDEX `ao_uq_index` (`user_id`,`open_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='微信账号账号绑定';



