/*
 * Copyright 2016-2022 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.system.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mendmix.cache.CacheUtils;
import com.mendmix.common.model.IdNamePair;
import com.oneplatform.system.dao.mapper.IdNameMappingMapper;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakinge</a>
 * @date Jul 24, 2022
 */
@Service
public class IdNameMappingService {

	@Autowired
	private IdNameMappingMapper idNameMappingMapper;

	
	public List<IdNamePair> findList(String type, List<String> ids){
		List<IdNamePair> idNames;
		Map<String, Object> cacheValues = CacheUtils.getMapValues(type, ids);
		if(!cacheValues.isEmpty()) {
			idNames = new ArrayList<>(ids.size());
			cacheValues.forEach( (k,v) -> {
				idNames.add(new IdNamePair(k, v.toString()));
			} );
		}else {
			idNames = idNameMappingMapper.findIdNameMappings(type, ids);
		}
		return idNames;
	}
}
