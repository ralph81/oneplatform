/*
 * Copyright 2016-2022 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.system.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mendmix.common.model.IdNamePair;

/**
 * 
 * <br>
 * Class Name   : IdNameMappingMapper
 *
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @version 1.0.0
 * @date May 21, 2022
 */
public interface IdNameMappingMapper {

	int insertIdNameMappings(@Param("type") String type,@Param("items") List<IdNamePair> items);
	
	List<IdNamePair> findIdNameMappings(@Param("type") String type,@Param("ids") List<String> ids);
}
