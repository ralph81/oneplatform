package com.oneplatform.system.dao.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.system.dao.entity.SequenceRuleEntity;

public interface SequenceRuleEntityMapper extends BaseMapper<SequenceRuleEntity, String> {

	
	@Select("SELECT * FROM sequence_rules WHERE system_id=#{systemId} AND code=#{code} LIMIT 1")
	@ResultMap("BaseResultMap")
	SequenceRuleEntity findByCode(@Param("systemId") String systemId,@Param("code") String code);
	
	@Select("SELECT last_sequence FROM sequence_rules WHERE system_id=#{systemId} AND code=#{code} LIMIT 1")
	@ResultType(Integer.class)
	Integer findLastSequence(@Param("systemId") String systemId,@Param("code") String code);
	
	@Update("UPDATE sequence_rules SET last_sequence=last_sequence+#{incr} WHERE system_id=#{systemId} AND code=#{code} AND last_sequence=#{expectLastSequence}")
	@ResultType(Integer.class)
	int updateLastSequence(@Param("systemId") String systemId,@Param("code") String code,@Param("incr") int incr,@Param("expectLastSequence") int expectLastSequence);

	@Select("SELECT last_sequence FROM sequence_rules WHERE id=#{id} for update")
	@ResultMap("BaseResultMap")
	SequenceRuleEntity getByIdForUpdate(String id);
	
	@Update("UPDATE sequence_rules SET last_sequence=#{expectLastSequence} WHERE id=#{id}")
	@ResultType(Integer.class)
	int updateLastSequenceById(@Param("id") String id,@Param("expectLastSequence") int expectLastSequence);

	
}