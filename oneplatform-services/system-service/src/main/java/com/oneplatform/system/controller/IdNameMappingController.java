/*
 * Copyright 2016-2022 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.system.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.model.IdNamePair;
import com.oneplatform.connect.api.system.IdNameMappingApi;
import com.oneplatform.system.service.IdNameMappingService;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakinge</a>
 * @date Jul 24, 2022
 */
@RestController
public class IdNameMappingController implements IdNameMappingApi {

	@Autowired
	private IdNameMappingService idNameMappingService;
	
	@Override
	public Map<String, String> findIdNames(String type, List<String> ids) {
		List<IdNamePair> list = idNameMappingService.findList(type, ids);
		return list.stream().collect(Collectors.toMap(IdNamePair::getId, IdNamePair::getName));
	}

	

}
