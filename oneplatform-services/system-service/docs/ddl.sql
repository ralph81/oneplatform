DROP TABLE IF EXISTS `id_name_mappings`;
CREATE TABLE `id_name_mappings` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `name` varchar(64) NOT NULL COMMENT '名称',
  `type` varchar(32) NOT NULL COMMENT '类型',
  PRIMARY KEY (`id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='通用冗余字段';


DROP TABLE IF EXISTS `action_logs`;
CREATE TABLE `action_logs` (
  `id` varchar(32) NOT NULL,
  `system_id` varchar(32)  NOT NULL, 
  `tenant_id` varchar(32)  DEFAULT NULL,
  `action_name` varchar(64) NOT NULL,
  `action_key` varchar(128) NOT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `user_name` varchar(32) DEFAULT NULL,
  `client_type` varchar(32) DEFAULT NULL,
  `platform_type` varchar(32) DEFAULT NULL,
  `module_id` varchar(32) DEFAULT NULL,
  `request_ip` varchar(20) DEFAULT NULL, 
  `request_id` varchar(32) DEFAULT NULL,
  `request_at` datetime DEFAULT NULL,
  `query_parameters` varchar(500) DEFAULT NULL,
  `request_data` TEXT DEFAULT NULL,
  `response_data` TEXT DEFAULT NULL,
  `response_code` int(3) DEFAULT NULL,
  `use_time` int(6) DEFAULT NULL,
  `exceptions` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志';

DROP TABLE IF EXISTS `sequence_rules`;
CREATE TABLE `sequence_rules` (
  `id` varchar(32) NOT NULL,
  `name` varchar(32) DEFAULT NULL COMMENT '名称',
  `code` varchar(32) DEFAULT NULL COMMENT '序列业务编号',
  `prefix` varchar(32) DEFAULT NULL COMMENT '前缀',
  `time_expr` varchar(32) DEFAULT NULL COMMENT '时间表达式',
  `seq_length` int(1) NOT NULL DEFAULT '6' COMMENT '自增序列位数',
  `first_sequence` int(10) NOT NULL DEFAULT '1' COMMENT '初始自增序列值',
  `last_sequence` int(10) NOT NULL DEFAULT '1' COMMENT '最新自增序列值',
  `random_type` ENUM('CHAR','NUMBER','ANY') DEFAULT 'any' COMMENT '随机字符串类型',
  `random_length` int(1) DEFAULT 0 COMMENT '随机字符串长度',
  `system_id` varchar(32)  NOT NULL, 
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '激活状态（1：激活，0：未激活）',
  `deleted` TINYINT(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL COMMENT '更新人姓名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='序列生成规则';
