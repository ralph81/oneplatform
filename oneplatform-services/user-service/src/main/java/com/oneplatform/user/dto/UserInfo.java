package com.oneplatform.user.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserInfo extends BaseDto{

	private String name;
	
	private String nickname;
	
	private String realname;

    private String mobile;
    
    private String avatar;
    
    @JsonIgnore
    private String password;
    
    private String staffName;
    private String staffNo;
    
    private List<UserScope> scopes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffNo() {
		return staffNo;
	}

	public void setStaffNo(String staffNo) {
		this.staffNo = staffNo;
	}

	public List<UserScope> getScopes() {
		return scopes;
	}

	public void setScopes(List<UserScope> scopes) {
		this.scopes = scopes;
	}

	
}
