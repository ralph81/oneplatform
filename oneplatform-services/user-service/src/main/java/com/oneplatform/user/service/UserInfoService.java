package com.oneplatform.user.service;

import java.util.List;
import java.util.stream.Collectors;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.util.BeanUtils;
import com.mendmix.common.util.FormatValidateUtils;
import com.mendmix.mybatis.plugin.pagination.PageExecutor;
import com.oneplatform.user.dao.entity.UserInfoEntity;
import com.oneplatform.user.dao.entity.UserScopeEntity;
import com.oneplatform.user.dao.mapper.UserInfoEntityMapper;
import com.oneplatform.user.dao.mapper.UserScopeEntityMapper;
import com.oneplatform.user.dto.UserInfo;
import com.oneplatform.user.dto.UserScope;
import com.oneplatform.user.dto.param.UserQueryParam;

@Service
public class UserInfoService {

	@Autowired
	private UserInfoEntityMapper  userMapper;
	
	@Autowired
	private UserScopeEntityMapper userScopeMapper;
	
	public void addUser(UserInfoEntity entity){
		entity.setPassword(BCrypt.hashpw(entity.getPassword(), BCrypt.gensalt()));
		userMapper.insertSelective(entity);
	}
	
	public void updateUser(UserInfoEntity entity){
		userMapper.updateByPrimaryKeySelective(entity);
	}
	

	public UserInfoEntity findById(String id){
		return userMapper.selectByPrimaryKey(id);
	}


	public List<UserInfoEntity> findListByParam(UserQueryParam param){
		return userMapper.findListByParam(param);
	}
	
	public Page<UserInfo> pageQuery(PageParams pageParams, UserQueryParam example) {
	    
        return PageExecutor.pagination(pageParams, new PageExecutor.ConvertPageDataLoader<UserInfoEntity, UserInfo>() {
            @Override
            public UserInfo convert(UserInfoEntity apiResourceEntity) {
                return BeanUtils.copy(apiResourceEntity,UserInfo.class);
            }

            @Override
            public List<UserInfoEntity> load() {
                return userMapper.findListByParam(example == null ? new UserQueryParam() : example);
            }
        });
    }
	
	public UserInfo findByAccount(String user) {
		UserInfoEntity entity;
		if(FormatValidateUtils.isMobile(user)) {
			entity = userMapper.findByMobile(user);
		} else {
			entity = userMapper.findByName(user);
		}
		if(entity == null || !entity.getEnabled()) {
			throw new MendmixBaseException("账号不存在或者已停用");
		}
		
		UserInfo acountDto = BeanUtils.copy(entity, UserInfo.class);

		return acountDto;
	}
	
	public UserInfo validateUser(String user,String password) {
		UserInfo acountDto = findByAccount(user);
		if(!BCrypt.checkpw(password, acountDto.getPassword())) {
			throw new MendmixBaseException("账号不存在或密码错误");
		}
		return acountDto;
	}
	
	public List<UserScope> findUserScopes(String userId){
		List<UserScopeEntity> scopes = userScopeMapper.findByUserId(userId);
		return scopes.stream().map(o -> {
			UserScope scope = new UserScope();
			scope.setPrincipalType(o.getPrincipalType());
			scope.setPrincipalId(o.getPrincipalId());
			scope.setTenantId(o.getTenantId());
			scope.setAdmin(o.getIsAdmin());
			return scope;
		}).collect(Collectors.toList());
	}
}
