package com.oneplatform.user.dao.entity;

import javax.persistence.Column;
import javax.persistence.Table;

import com.oneplatform.user.dao.StandardBaseEntity;

@Table(name = "user_scope")
public class UserScopeEntity extends StandardBaseEntity {
   
    @Column(name = "user_id")
    private String userId;

    @Column(name = "tenant_id")
    private String tenantId;

    @Column(name = "principal_type")
    private String principalType;

    @Column(name = "principal_id")
    private String principalId;

    /**
     * 是否管理员
     */
    @Column(name = "is_admin")
    private Boolean isAdmin;

    /**
     * 是否默认
     */
    @Column(name = "is_default")
    private Boolean isDefault;

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return tenant_id
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * @param tenantId
     */
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * @return principal_type
     */
    public String getPrincipalType() {
        return principalType;
    }

    /**
     * @param principalType
     */
    public void setPrincipalType(String principalType) {
        this.principalType = principalType;
    }

    /**
     * @return principal_id
     */
    public String getPrincipalId() {
        return principalId;
    }

    /**
     * @param principalId
     */
    public void setPrincipalId(String principalId) {
        this.principalId = principalId;
    }

    /**
     * 获取是否管理员
     *
     * @return is_admin - 是否管理员
     */
    public Boolean getIsAdmin() {
        return isAdmin;
    }

    /**
     * 设置是否管理员
     *
     * @param isAdmin 是否管理员
     */
    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * 获取是否默认
     *
     * @return is_default - 是否默认
     */
    public Boolean getIsDefault() {
        return isDefault;
    }

    /**
     * 设置是否默认
     *
     * @param isDefault 是否默认
     */
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

}