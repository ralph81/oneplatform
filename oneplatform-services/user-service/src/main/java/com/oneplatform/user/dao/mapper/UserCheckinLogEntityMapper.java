package com.oneplatform.user.dao.mapper;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.user.dao.entity.UserCheckinLogEntity;

public interface UserCheckinLogEntityMapper extends BaseMapper<UserCheckinLogEntity, String> {
}