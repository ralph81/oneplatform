package com.oneplatform.user.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mendmix.mybatis.core.BaseEntity;

@Table(name = "user_checkin_log")
public class UserCheckinLogEntity extends BaseEntity {
    @Id
    private String id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "tenant_id")
    private String tenantId;

    /**
     * 连续签到次数
     */
    @Column(name = "continuity_nums")
    private Integer continuityNums;

    @Column(name = "checkin_at")
    private Date checkinAt;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return tenant_id
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * @param tenantId
     */
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 获取连续签到次数
     *
     * @return continuity_nums - 连续签到次数
     */
    public Integer getContinuityNums() {
        return continuityNums;
    }

    /**
     * 设置连续签到次数
     *
     * @param continuityNums 连续签到次数
     */
    public void setContinuityNums(Integer continuityNums) {
        this.continuityNums = continuityNums;
    }

    /**
     * @return checkin_at
     */
    public Date getCheckinAt() {
        return checkinAt;
    }

    /**
     * @param checkinAt
     */
    public void setCheckinAt(Date checkinAt) {
        this.checkinAt = checkinAt;
    }
}