package com.oneplatform.user.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.mendmix.mybatis.core.BaseMapper;
import com.mendmix.mybatis.plugin.cache.annotation.Cache;
import com.oneplatform.user.dao.entity.UserInfoEntity;
import com.oneplatform.user.dto.param.UserQueryParam;

public interface UserInfoEntityMapper extends BaseMapper<UserInfoEntity, String> {

	List<UserInfoEntity> findListByParam(UserQueryParam param);
	
	@Cache(uniqueIndex = true)
    @Select("SELECT * FROM user_info WHERE mobile = #{mobile} LIMIT 1")
    @ResultMap("BaseResultMap")
    UserInfoEntity findByMobile(String mobile);
	
	@Cache(uniqueIndex = true)
    @Select("SELECT * FROM user_info WHERE name = #{name} LIMIT 1")
    @ResultMap("BaseResultMap")
    UserInfoEntity findByName(String name);

    @Cache
    @Select("SELECT a.* FROM user_info a INNER JOIN user_scope s ON a.id = s.user_id WHERE AND s.principal_type = #{principalType} AND s.principal_id = #{principalId} LIMIT 1 ")
    @ResultMap("BaseResultMap")
    UserInfoEntity findBypPrincipal(@Param("principalType") String principalType,@Param("principalId") String principalId);
    
}