package com.oneplatform.user.dao.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Table;

import com.oneplatform.user.dao.StandardBaseEntity;

@Table(name = "user_wallet")
public class UserWalletEntity extends StandardBaseEntity {
   
    @Column(name = "user_id")
    private String userId;
    
    @Column(name = "tenant_id")
    private String tenantId;

    /**
     * 余额，平台币，积分
     */
    @Column(name = "wallet_type")
    private String walletType;

    /**
     * 可用的
     */
    private BigDecimal available;

    /**
     * 冻结的
     */
    private BigDecimal frozen;

    private String sign;

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    

    public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
     * 获取余额，平台币，积分
     *
     * @return wallet_type - 余额，平台币，积分
     */
    public String getWalletType() {
        return walletType;
    }

    /**
     * 设置余额，平台币，积分
     *
     * @param walletType 余额，平台币，积分
     */
    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    /**
     * 获取可用的
     *
     * @return available - 可用的
     */
    public BigDecimal getAvailable() {
        return available;
    }

    /**
     * 设置可用的
     *
     * @param available 可用的
     */
    public void setAvailable(BigDecimal available) {
        this.available = available;
    }

    /**
     * 获取冻结的
     *
     * @return frozen - 冻结的
     */
    public BigDecimal getFrozen() {
        return frozen;
    }

    /**
     * 设置冻结的
     *
     * @param frozen 冻结的
     */
    public void setFrozen(BigDecimal frozen) {
        this.frozen = frozen;
    }

    /**
     * @return sign
     */
    public String getSign() {
        return sign;
    }

    /**
     * @param sign
     */
    public void setSign(String sign) {
        this.sign = sign;
    }

}