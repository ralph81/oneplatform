DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` varchar(32)  NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `nickname` varchar(32) DEFAULT NULL,
  `realname` varchar(32) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `id_number` varchar(20) DEFAULT NULL,
  `gender` ENUM('female', 'male','unknow') DEFAULT 'unknow',
  `birtyday` date DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `is_certified` tinyint(1) DEFAULT 0 COMMENT '是否已认证',
  `level` int(2) DEFAULT 1 COMMENT '用户等级',
  `enabled` tinyint(1) DEFAULT 1,
  `deleted` tinyint(1) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';


DROP TABLE IF EXISTS `user_scope`;
CREATE TABLE `user_scope` (
  `id` varchar(32)  NOT NULL,
  `user_id` varchar(32)  NOT NULL ,
  `tenant_id` varchar(32) DEFAULT NULL ,
  `principal_type` varchar(32)  NOT NULL ,
  `principal_id` varchar(32)  NOT NULL ,
  `is_admin` tinyint(1) DEFAULT 0  COMMENT '是否管理员',
  `is_default` tinyint(1) DEFAULT 0  COMMENT '是否默认',
  `enabled` tinyint(1) DEFAULT 1,
  `deleted` tinyint(1) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户范围';


DROP TABLE IF EXISTS `user_wallet`;
CREATE TABLE `user_wallet` (
  `id` varchar(32)  NOT NULL,
  `user_id` varchar(32)  NOT NULL, 
  `tenant_id` varchar(32) DEFAULT NULL ,
  `wallet_type` ENUM('balance', 'coin','point') DEFAULT NULL COMMENT '余额，平台币，积分',
  `available` DECIMAL(10,2) DEFAULT 0 COMMENT '可用的',
  `frozen` DECIMAL(10,2) DEFAULT 0 COMMENT '冻结的',
  `sign` char(32) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT 1,
  `deleted` tinyint(1) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
   UNIQUE INDEX `ao_uq_index` (`user_id`,`wallet_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户钱包';


DROP TABLE IF EXISTS `user_wallet_log`;
CREATE TABLE `user_wallet_log` (
  `id` varchar(32)  NOT NULL,
  `user_id` varchar(32)  NOT NULL, 
  `tenant_id` varchar(32) DEFAULT NULL ,
  `wallet_id` varchar(32)  NOT NULL, 
  `order_no` varchar(32) DEFAULT NULL,
  `trade_name` varchar(64) DEFAULT NULL,
  `wallet_type` ENUM('balance', 'coin','point') DEFAULT NULL COMMENT '余额，平台币，积分',
  `sub_type` ENUM('available', 'frozen') DEFAULT NULL COMMENT '可用的，冻结的',
  `trade_type` ENUM('out', 'in','freeze','unfreeze') DEFAULT NULL COMMENT '支出，收入，冻结，解冻',
  `amount` decimal(12,2) DEFAULT NULL,
  `current_available` DECIMAL(9,2) DEFAULT 0 COMMENT '当前可用的',
  `current_frozen` DECIMAL(10,2) DEFAULT 0 COMMENT '当前冻结的',
  `sign` char(32) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `hidden` tinyint(1) DEFAULT '0' COMMENT '是否隐藏(不对外显示)',
  `trade_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='钱包流水记录';


DROP TABLE IF EXISTS `user_checkin_log`;
CREATE TABLE `user_checkin_log` (
  `id` varchar(32)  NOT NULL,
  `user_id` varchar(32)  NOT NULL ,
  `tenant_id` varchar(32) DEFAULT NULL ,
  `continuity_nums` int(2) DEFAULT 0  COMMENT '连续签到次数',
  `checkin_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户签到';
