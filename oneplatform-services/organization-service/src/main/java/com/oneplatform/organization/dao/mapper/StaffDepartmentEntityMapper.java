package com.oneplatform.organization.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.organization.dao.entity.StaffDepartmentEntity;

public interface StaffDepartmentEntityMapper extends BaseMapper<StaffDepartmentEntity, String> {
	
	@Select("SELECT * FROM staff_departments WHERE department_id=#{deptId} AND staff_id=#{staffId} AND enabled = 1 ")
	@ResultMap("BaseResultMap")
	List<StaffDepartmentEntity> findByStaffAndDept(@Param("staffId")String staffId,@Param("deptId")String deptId);
	
	@Select("SELECT * FROM staff_departments WHERE staff_id=#{staffId} AND enabled = 1 ")
	@ResultMap("BaseResultMap")
	List<StaffDepartmentEntity> findByStaff(@Param("staffId")String staffId);
}