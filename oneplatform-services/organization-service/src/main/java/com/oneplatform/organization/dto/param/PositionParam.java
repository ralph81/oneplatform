package com.oneplatform.organization.dto.param;

/**
 * 
 * <br>
 * Class Name   : PositionParam
 */
public class PositionParam {

    private String id;
    
    private String name;
    
    private String departmentId;
   
    private java.util.Date effectDate;

    private java.util.Date invalidDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public java.util.Date getEffectDate() {
		return effectDate;
	}

	public void setEffectDate(java.util.Date effectDate) {
		this.effectDate = effectDate;
	}

	public java.util.Date getInvalidDate() {
		return invalidDate;
	}

	public void setInvalidDate(java.util.Date invalidDate) {
		this.invalidDate = invalidDate;
	}

    
}