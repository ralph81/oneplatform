package com.oneplatform.permission.dao.mapper;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.permission.dao.entity.BizSystemPortalEntity;


public interface BizSystemPortalEntityMapper extends BaseMapper<BizSystemPortalEntity,Integer> {
	
}
