package com.oneplatform.permission.dto.param;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;


public class BizSystemParam {

    private Integer id;

    @NotBlank(message = "系统编码不能为空")
    private String code;

    @NotBlank(message = "系统名称不能为空")
    private String name;

    private String alias;

    private String icon;

    private String description;

    private String level;

    private String type;

    private String state;

    private Date launchTime;

    private String consumer;

    private String bizDepartmentCode;

    private String bizDepartmentName;

    private String supplierName;

    private String supplierLinkman;

    private String supplierPhone;

    private String chargeDepartmentCode;

    private String chargeDepartmentName;
    
    private Boolean standardAuthMode = false;

    private Boolean operational;

    private Boolean internal;

    private String chargeUserCode;

    private String chargeUserName;

    private List<UserRoleParam> roleList;

    private List<String> domainList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
	public Boolean getStandardAuthMode() {
		return standardAuthMode;
	}

	public void setStandardAuthMode(Boolean standardAuthMode) {
		this.standardAuthMode = standardAuthMode;
	}

    public String getChargeUserName() {
        return chargeUserName;
    }

    public void setChargeUserName(String chargeUserName) {
        this.chargeUserName = chargeUserName;
    }

    public String getChargeDepartmentName() {
        return chargeDepartmentName;
    }

    public void setChargeDepartmentName(String chargeDepartmentName) {
        this.chargeDepartmentName = chargeDepartmentName;
    }

    public String getChargeDepartmentCode() {
        return chargeDepartmentCode;
    }

    public void setChargeDepartmentCode(String chargeDepartmentCode) {
        this.chargeDepartmentCode = chargeDepartmentCode;
    }

    public String getChargeUserCode() {
        return chargeUserCode;
    }

    public void setChargeUserCode(String chargeUserCode) {
        this.chargeUserCode = chargeUserCode;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getLaunchTime() {
        return launchTime;
    }

    public void setLaunchTime(Date launchTime) {
        this.launchTime = launchTime;
    }

    public String getConsumer() {
        return consumer;
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

    public String getBizDepartmentCode() {
        return bizDepartmentCode;
    }

    public void setBizDepartmentCode(String bizDepartmentCode) {
        this.bizDepartmentCode = bizDepartmentCode;
    }

    public String getBizDepartmentName() {
        return bizDepartmentName;
    }

    public void setBizDepartmentName(String bizDepartmentName) {
        this.bizDepartmentName = bizDepartmentName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierLinkman() {
        return supplierLinkman;
    }

    public void setSupplierLinkman(String supplierLinkman) {
        this.supplierLinkman = supplierLinkman;
    }

    public String getSupplierPhone() {
        return supplierPhone;
    }

    public void setSupplierPhone(String supplierPhone) {
        this.supplierPhone = supplierPhone;
    }

    public List<String> getDomainList() {
        return domainList;
    }

    public void setDomainList(List<String> domainList) {
        this.domainList = domainList;
    }

    public List<UserRoleParam> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<UserRoleParam> roleList) {
        this.roleList = roleList;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Boolean getOperational() {
        return operational;
    }

    public void setOperational(Boolean operational) {
        this.operational = operational;
    }

    public Boolean getInternal() {
        return internal;
    }

    public void setInternal(Boolean internal) {
        this.internal = internal;
    }
}
