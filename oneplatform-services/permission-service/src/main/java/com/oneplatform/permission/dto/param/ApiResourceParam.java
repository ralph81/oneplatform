package com.oneplatform.permission.dto.param;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ApiResourceParam {

    private Integer id;

    /**
     * 对应应用id
     */
    @NotNull(message = "关联应用不能为空")
    private Integer moduleId;

    /**
     * 资源名称
     */
    @NotBlank(message = "接口名称不能为空")
    private String name;

    /**
     * uri
     */
    @NotBlank(message = "uri不能为空")
    private String uri;

    /**
     * 请求方式：GET,POST
     */
    private String httpMethod;

    /**
     * 授权类型
     */
    private String permissionLevel;
    
    private Boolean openapi;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

	public String getPermissionLevel() {
		return permissionLevel;
	}

	public void setPermissionLevel(String permissionLevel) {
		this.permissionLevel = permissionLevel;
	}

	public Boolean getOpenapi() {
		return openapi;
	}

	public void setOpenapi(Boolean openapi) {
		this.openapi = openapi;
	}

    

}