/*
 * Copyright 2016-2022 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.connect.api.user;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mendmix.common.model.IdParam;
import com.oneplatform.connect.dto.user.UserInfoParam;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakinge</a>
 * @date Jul 24, 2022
 */
@FeignClient(name = "oneplatform-user-svc" ,contextId = "userApi" , path = "/")
public interface UserApi {

	@PostMapping(value = "add")
	@ResponseBody
	IdParam<String> add(@RequestBody UserInfoParam param);
	
	@PostMapping(value = "update")
	@ResponseBody
	void update(@RequestBody UserInfoParam param);
}
