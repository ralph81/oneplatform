import * as api from './api';
import { dict,compute } from '@fast-crud/fast-crud';
import { orgTypeDict,boolDict } from '@/enums/globalDict';
import { useMessage,useDialog } from 'naive-ui';
export default function ({ expose }) {
  const pageRequest = async (query) => {
    return await api.GetList(query);
  };
  const editRequest = async ({ form, row }) => {
    form.id = row.id;
    return await api.UpdateObj(form);
  };
  const delRequest = async ({ row }) => {
    return await api.DelObj(row.id);
  };

  const addRequest = async ({ form }) => {
    return await api.AddObj(form);
  };
  
  const message = useMessage();
  const dialog = useDialog();
  return {
    crudOptions: {
      request: {
        pageRequest,
        addRequest,
        editRequest,
        delRequest,
      },
      columns: {
        departmentId: {
          title: '所属部门',
          key: 'departmentId',
          search: { show: false },
          column: {
            show: false,
          },
          form: {
            show: true,
          },
        },
        departmentName: {
          title: '所属部门',
          key: 'departmentName',
          search: { show: false },
          column: {
            width: 200,
          },
          form: {
            show: false,
          },
        },
        name: {
          title: '名称',
          key: 'name',
          type: 'text',
          search: { show: true },
          column: {
            width: 120,
          },
          form: {
            show: true,
          },
        },
        enabled: {
          title: '状态',
          search: { show: true },
          type: 'dict-switch',
          form: {show: false},
          column: {
            width: 80,
            component: {
              name: 'fs-dict-switch',
              vModel: 'value',
              'onUpdate:value': compute((context) => {
                return async () => {
                  let btntxt = context.row.enabled ? '启用' : '禁用';
                  await dialog.info({
                    title: '确认',
                    content: `确定${btntxt}该岗位么？`,
                    positiveText: '确定',
                    negativeText: '取消',
                    async onPositiveClick() {
                      await api.toggleObj(context.row.id);
                      message.info('操作成功');
                    },
                  });
                };
              }),
            },
          },
          dict: dict({
            data: [
              { value: true, label: '启用' },
              { value: false, label: '禁用' },
            ],
          }),
        },
        createdAt: {
          title: '创建时间',
          key: 'createdAt',
          type: 'text',
          column: {
            width: 120,
          },
          form: {
            show: false,
          },
        },
      },
    },
  };
}
