import { ref } from 'vue';
import * as api from '../api';
import { dict, compute } from '@fast-crud/fast-crud';
import { useMessage } from 'naive-ui';
export default function ({ expose }) {
  const pageRequest = async (query) => {
    return await api.GetList(query);
  };
  const message = useMessage();

  const currentStaff = ref();
  
  const onCurrentRowChange = (row) => {
    currentStaff.value = row;
  }
  
  return {
    currentStaff,
    crudOptions: {
      request: {
        pageRequest,
      },
      search: {
        show: true,
      },
      actionbar: {
        show: false,
      },
      toolbar: {
        show: false,
      },
      rowHandle: {
        show: false,
      },
      table: {
        rowProps: (row) => {
          const clazz = currentStaff.value && row.id === currentStaff.value.id ? 'fs-current-row' : '';
          return {
            style: 'cursor: pointer;',
            onClick() {
              onCurrentRowChange(row);
            },
            class: clazz,
          };
        },
      },
      columns: {
        id: {
          title: 'ID',
          key: 'id',
          type: 'text',
          column: {
            show: false
          },
        },
        name: {
          title: '姓名',
          key: 'name',
          type: 'text',
          search: { show: true },
          column: {
            width: 70,
          },
          form: {
            show: true,
          },
        },
        code: {
          title: '员工号',
          key: 'code',
          type: 'text',
          column: {
            width: 80,
          },
        },
      },
    },
  };
}
