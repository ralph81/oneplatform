import * as api from './api';
import { dict } from '@fast-crud/fast-crud';
import { orgTypeDict,boolDict } from '@/enums/globalDict';
import { useMessage } from 'naive-ui';
export default function ({ expose }) {
  const pageRequest = async (query) => {
    let data = await api.GetList(query);
    return {
      pageNo: 1,
      pageSize: data.length,
      data: data,
    };
  };
  const editRequest = async ({ form, row }) => {
    form.id = row.id;
    return await api.UpdateObj(form);
  };
  const delRequest = async ({ row }) => {
    return await api.DelObj(row.id);
  };

  const addRequest = async ({ form }) => {
    return await api.AddObj(form);
  };
  
  const message = useMessage();
  return {
    crudOptions: {
      request: {
        pageRequest,
        addRequest,
        editRequest,
        delRequest,
      },
      initialForm:{
         orgType: 'sub',
         isVirtual: false,
      },
      columns: {
        // parentId: {
        //   title: '父级部门',
        //   key: 'parentId',
        //   type: 'dict-tree',
        //   search: { show: false },
        //   dict: dict({
        //     isTree: true,
        //     url: '/org/dept/tree',
        //     value: 'id',
        //     label: 'name',
        //   }),
        //   form: {
        //     component: {
        //       keyField: 'id',
        //       labelField: 'name',
        //     },
        //   },
        //   column: {
        //     show: false,
        //   },
        //   form: {
        //     show: true,
        //   },
        // },
        parentId: {
          title: '父级部门',
          key: 'parentId',
          search: { show: true },
          column: {
            show: false,
          },
          form: {
            show: true,
          },
        },
        name: {
          title: '名称',
          key: 'name',
          type: 'text',
          search: { show: true },
          column: {
            width: 120,
          },
          form: {
            show: true,
          },
        },
        code: {
          title: '编码',
          key: 'code',
          type: 'text',
          column: {
            width: 120,
          },
          form: {
            show: false,
          },
        },
        orgType: {
          title: '组织类型',
          key: 'orgType',
          dict: orgTypeDict,
          type: 'dict-select',
          search: { show: true },
          column: {
            width: 120,
          },
          form: {
            show: true,
            firstDefault: 'sub',
            component: {
              dict: {
                onReady({ dict, form }) {
                  form.firstDefault = 'sub';
                }
              }
            },
          },
        },
        isVirtual: {
          title: '虚拟组织',
          key: 'isVirtual',
          dict: boolDict,
          type: 'dict-radio',
          search: { show: true },
          column: {
            width: 120,
          },
          form: {
            show: true,
          },
        },
        leaderId: {
          title: '负责人',
          key: 'leaderId',
          component:{ 
             name:'fs-values-format'
          },
          column: {
            width: 80,
          },
          form: {
            show: true,
          },
        },
        sortIndex: {
          title: '排序',
          key: 'sortIndex',
          type: 'number',
          column: {
            width: 60,
          },
          form: {
            show: true,
          },
        },
        createdAt: {
          title: '创建时间',
          key: 'createdAt',
          type: 'text',
          column: {
            width: 120,
          },
          form: {
            show: false,
          },
        },
      },
      pagination:{show: false},
    },
  };
}
