/*
 * Copyright 2016-2022 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.support.actionlog;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mendmix.common.http.HttpRequestEntity;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.model.PageQueryRequest;
import com.mendmix.common.util.JsonUtils;
import com.mendmix.logging.integrate.ActionLog;
import com.mendmix.logging.integrate.ActionLogQueryParam;
import com.mendmix.logging.integrate.LogStorageProvider;

/**
 * 
 * <br>
 * Class Name   : SysApiLogStorageProvider
 *
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @version 1.0.0
 * @date May 21, 2022
 */
@Component
public class SysApiLogStorageProvider implements LogStorageProvider {

	@Value("${actionlog.service.baseUrl:http://oneplatform-system-svc}/actionlog/add")
	private String addUrl;
	
	@Value("${actionlog.service.baseUrl:http://oneplatform-system-svc}/actionlog/list")
	private String listUrl;
	
	@Value("${actionlog.service.baseUrl:http://oneplatform-system-svc}/actionlog/details")
	private String detailsUrl;
	
	@Override
	public void storage(ActionLog log) {

		String json = JsonUtils.toJson(log);
		HttpRequestEntity requestEntity = HttpRequestEntity.post(addUrl).body(json);
		requestEntity.execute();
	}

	@Override
	public Page<ActionLog> pageQuery(PageParams pageParam, ActionLogQueryParam queryParam) {
		PageQueryRequest<ActionLogQueryParam> pageQueryRequest;
		pageQueryRequest = new PageQueryRequest<>(pageParam.getPageNo(), pageParam.getPageSize(), queryParam);
		return HttpRequestEntity.post(listUrl).body(JsonUtils.toJson(pageQueryRequest)).execute().toPage(ActionLog.class);
	}

	@Override
	public ActionLog getDetails(String id) {
		return HttpRequestEntity.get(detailsUrl).queryParam("id", id).execute().toObject(ActionLog.class);
	}

}
