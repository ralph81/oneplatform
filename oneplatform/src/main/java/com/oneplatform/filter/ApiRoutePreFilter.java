package com.oneplatform.filter;

import org.springframework.stereotype.Component;

import com.mendmix.gateway.filter.AbstracRequestFilter;
import com.oneplatform.filter.prehandler.SystemIdQueryParamHandler;

/**
 * 
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年6月12日
 */
@Component
public class ApiRoutePreFilter extends AbstracRequestFilter {

	public ApiRoutePreFilter() {
		super(new SystemIdQueryParamHandler());
	}
	


}
