package com.oneplatform.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.AuthUser;
import com.mendmix.common.model.WrapperResponse;
import com.mendmix.security.SecurityDelegating;
import com.mendmix.security.model.UserSession;
import com.oneplatform.support.auth.LoginParam;

@Controller
public class UserAuthController {

	@RequestMapping(value = "login", method = RequestMethod.POST)
	@ApiMetadata(permissionLevel = PermissionLevel.Anonymous,actionLog = false)
    public @ResponseBody WrapperResponse<UserSession> doLogin(@RequestBody LoginParam param) {
		UserSession session = SecurityDelegating.doAuthentication(param.getUserType(),param.getAccount(), param.getPassword());
		return WrapperResponse.success(session);
	} 
	
	@RequestMapping(value = "logout", method = RequestMethod.POST)
	@ApiMetadata(permissionLevel = PermissionLevel.Anonymous,actionLog = false)
    public @ResponseBody WrapperResponse<Void> logout() {
		SecurityDelegating.doLogout();
		return WrapperResponse.success();
	} 
	
	@RequestMapping(value = "current_user", method = RequestMethod.GET)
	@ApiMetadata(permissionLevel = PermissionLevel.LoginRequired,actionLog = false)
    public @ResponseBody WrapperResponse<AuthUser> getCurrentUser() {
		UserSession session = SecurityDelegating.getAndValidateCurrentSession();
		return WrapperResponse.success(session.getUser());
	} 
}
